package it.source.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText login;
    EditText email;
    EditText phone;
    EditText password;
    EditText repeatPassword;

    Button check;

    TextView loginError;
    TextView emailError;
    TextView phoneError;
    TextView passwordError;
    TextView passwordRepeatError;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        login = findViewById(R.id.login);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        password = findViewById(R.id.password);
        repeatPassword = findViewById(R.id.repeat_password);

        check = findViewById(R.id.check);

        loginError = findViewById(R.id.login_error);
        emailError = findViewById(R.id.email_error);
        phoneError = findViewById(R.id.phone_error);
        passwordError = findViewById(R.id.password_error);
        passwordRepeatError = findViewById(R.id.passwordRepeat_error);



        check.setOnClickListener(new View.OnClickListener() {
            //@override
            public void onClick(View v) {
                if (!hasEmptyEditText() && !equalPasswordAndRepeatPassword()) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Success")
                            .setMessage("Validation was Successful")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }
            }
        });
    }



    private boolean hasEmptyEditText() {
        boolean hasError = false;
        if (login.getText().toString().isEmpty()) {
            hasError = true;
            loginError.setVisibility(View.VISIBLE);
        } else {
            loginError.setVisibility(View.GONE);
        }
        if (email.getText().toString().isEmpty()) {
            hasError = true;
            emailError.setVisibility(View.VISIBLE);
        } else {
            emailError.setVisibility(View.GONE);
        }
        if (phone.getText().toString().isEmpty()) {
            hasError = true;
            phoneError.setVisibility(View.VISIBLE);
        } else {
            phoneError.setVisibility(View.GONE);
        }

        if (password.getText().toString().isEmpty()) {
            hasError = true;
            passwordError.setVisibility(View.VISIBLE);
        } else {
            passwordError.setVisibility(View.GONE);
        }
        if (repeatPassword.getText().toString().isEmpty()) {
            hasError = true;
            passwordRepeatError.setVisibility(View.VISIBLE);
        } else {
            passwordRepeatError.setVisibility(View.GONE);
        }
        return hasError;
    }

    public boolean equalPasswordAndRepeatPassword() {
        boolean hasError = false;
        if (!password.getText().toString().equals(repeatPassword.getText().toString())) {
            hasError = true;
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("ERROR")
                    .setMessage("Password mismatch")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                      @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                        }
                    }).show();
        }
        return hasError;
    }
}


//2. Создать приложение с одним экраном. 5 полей для ввода (EditText) и одну кнопку (Button).готово
// Поля (логин, email, номер телефона, пароль, повторный пароль) разместить по центру, друг под другом, кнопку - внизу экрана. готово
// По нажатию на кнопку реализовать валидацию полей (должны быть заполнены все поля, пароли должны совпадать).


// Уведомить пользователя о результате (например "валидация пройдена", "введите логин", "пароли не совпадают" пр.)
//2.1* Проверять валидность email и номера телефона в международном формате (+380501234567)